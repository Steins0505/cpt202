package com.cpt202.team.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cpt202.team.models.Appointment;

public interface APRepo extends JpaRepository<Appointment,Integer>{
    
}
