package com.cpt202.team.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpt202.team.models.Appointment;
import com.cpt202.team.repositories.APRepo;
import java.util.List;

@Service
public class APservice {
    @Autowired
    private APRepo apRepo;
    
    public Appointment newAppointment(Appointment appointment){
        return apRepo.save(appointment);

    }
    

    public List<Appointment> getAPList(){
        return apRepo.findAll();

    }

    
}
