package com.cpt202.team.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Appointment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String date;
    private String name;
    private int number;
    private String appointmentTime;
    private String vehicleRequest;
    private String status;
    

    public Appointment() {
    }
    
    public Appointment(int id,String date, String name, int number, String appointmentTime, String vehicleRequest,
            String status) {
        this.id = id;
        this.date = date;
        this.name = name;
        this.number = number;
        this.appointmentTime = appointmentTime;
        this.vehicleRequest = vehicleRequest;
        this.status = status;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public String getAppointmentTime() {
        return appointmentTime;
    }
    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }
    public String getVehicleRequest() {
        return vehicleRequest;
    }
    public void setVehicleRequest(String vehicleRequest) {
        this.vehicleRequest = vehicleRequest;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }


}
