package com.cpt202.team.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cpt202.team.TeamApplication;
import com.cpt202.team.models.Appointment;
import com.cpt202.team.services.APservice;

import java.util.List;
import java.util.ArrayList;

@Controller
@RequestMapping("/Appointment")
public class APController {
    
    @Autowired
    private APservice aPservice;
    

    @GetMapping("/list")
    public String getList(Model model){
        model.addAttribute("appointmentList", aPservice.getAPList());
        return "allAppointments";
    
    }
    
    @GetMapping("/add")
    public String addAppointment(Model model){
        model.addAttribute("appointment", new Appointment());
        return "addAppointment";
        
    }

    @PostMapping("/add")
    public String confirmNewAP(@ModelAttribute("appointment") Appointment appointment){
        aPservice.newAppointment(appointment);
        
        return "home";
    }
    
    @GetMapping("/edit")
    public String editAppointment(Model model){
        model.addAttribute("appointmentEdit", aPservice.getAPList());
        return "editAppointment";
    }


}
